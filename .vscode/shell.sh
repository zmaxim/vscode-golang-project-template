#!/bin/bash
workspaceRoot=$(cd $(dirname $0)/.. && pwd )

  mainGopath="$workspaceRoot/../_go"
extraGopaths="$workspaceRoot/../otherproject"

	GOPATH=$mainGopath

for p in $extraGopaths; do 
	GOPATH=$GOPATH:$p
done

	export GOPATH=$GOPATH:$workspaceRoot

exec bash "$@"
